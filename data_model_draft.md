# from sqlalchemy import create_engine, Column, Integer, String, JSON, ForeignKey
# from sqlalchemy.orm import declarative_base, relationship, sessionmaker


# ##############
# # Data Model #
# ##############

# Base = declarative_base()


# class Run(Base):
#     __tablename__ = "runs"

#     id = Column(Integer, primary_key=True)
#     case_id = Column(Integer, ForeignKey("cases.id"))

#     case = relationship("Case", back_populates="runs")


# class Case(Base):
#     __tablename__ = "cases"

#     id = Column(Integer, primary_key=True)
#     doe_id = Column(Integer, ForeignKey("does.id"))
#     parameters = Column(JSON)

#     doe = relationship("DOE", back_populates="cases")
#     runs = relationship("Run", back_populates="case")


# class DOE(Base):
#     __tablename__ = "does"

#     id = Column(Integer, primary_key=True)
#     name = Column(String)
#     shared_parameters = Column(JSON)

#     cases = relationship("Case", back_populates="doe")


# engine = create_engine("sqlite:///persistence.db", echo=True)
# Base.metadata.create_all(engine)
# Session = sessionmaker(bind=engine)