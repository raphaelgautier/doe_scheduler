import logging
from threading import Thread
from time import sleep

from doe_scheduler.client import DoESchedulerClient

num_workers = 10
cases_timeout = [7, 8, 9]
cases_failed = [10, 11, 12]


def dummy_worker(server, name):
    client = DoESchedulerClient(server, name, interval=3)
    while True:
        doe_name, case_number = client.get_case()
        if case_number is not None:
            if case_number in cases_timeout:
                logging.info(
                    f"Worker {name} simulating timeout for case {case_number}."
                )
                cases_timeout.remove(case_number)
                try:
                    raise RuntimeError("Simulating a code that errors out.")
                finally:
                    client._reset()  # needed for tests
            else:
                if case_number in cases_failed:
                    logging.info(
                        f"Worker {name} simulating failure for case {case_number}."
                    )
                    cases_failed.remove(case_number)
                    sleep(10)
                    client.report_failed()
                else:
                    logging.info(
                        f"Worker {name} simulating completion for case {case_number}."
                    )
                    sleep(10)
                    client.report_finished()
        else:
            logging.info(f"Worker {name} seems to be done here, bye!")
            break


if __name__ == "__main__":
    server = "http://localhost:3965/"
    # server = ...

    # Starting worker threads
    for worker_id in range(num_workers):
        thread = Thread(target=dummy_worker, args=(server, str(worker_id)))
        thread.start()
