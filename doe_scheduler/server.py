from dataclasses import dataclass, field
from datetime import datetime
from enum import Enum
import json
from logging import (
    basicConfig,
    getLogger,
    info,
    error,
    FileHandler,
    StreamHandler,
    INFO,
    ERROR,
)
from sys import stdout
from threading import Lock, Timer
from time import time
from typing import List

from flask import Flask, request, render_template, Response
from flask_cors import CORS
from waitress import serve

from doe_scheduler import logs_path

# Logging config
basicConfig(
    format="%(asctime)s %(levelname)s:%(message)s",
    level=ERROR,
    handlers={FileHandler(logs_path / "server.log", "a+"), StreamHandler(stdout)},
)


class State(Enum):
    QUEUED = 1
    RUNNING = 2
    FINISHED = 3
    FAILED = 4


@dataclass
class Case:
    id: int
    state: State
    timestamp: float = None
    worker_ids: List[int] = field(default_factory=list)


class DoESchedulerServer:
    def __init__(
        self,
        doe_name,
        cases_list,
        timeout=60,
        check_interval=10,
        host="0.0.0.0",
        port=3965,
    ) -> None:
        self._doe_name = doe_name
        self._cases = {
            case_id: Case(id=case_id, state=State.QUEUED) for case_id in cases_list
        }
        self._timeout = timeout
        self._check_interval = check_interval
        self._host = host
        self._port = port
        self._lock = Lock()
        self._timer = None

    def _get_cases_by_state(self, state):
        assert isinstance(state, State)
        return [case for case in self._cases.values() if case.state == state]

    # Thread used to check for timed out cases
    def _monitor_timed_out_cases(self):
        now = time()
        with self._lock:
            for case_id, case in self._cases.items():
                if case.state == State.RUNNING and now - case.timestamp > self._timeout:
                    case.state = State.FAILED
                    error((f"Case {case_id} (worker {case.worker_ids[-1]}) timed out."))

            queued_cases = self._get_cases_by_state(State.QUEUED)
            running_cases = self._get_cases_by_state(State.RUNNING)
            failed_cases = self._get_cases_by_state(State.FAILED)
            if queued_cases or running_cases or failed_cases:
                self._timer = Timer(self._check_interval, self._monitor_timed_out_cases)
                self._timer.start()
            else:
                info("All done here, so I stopped checking for timed out cases.")

    def start(self):
        # Flask app
        app = Flask(__name__)
        CORS(app)

        # Endpoints

        @app.route("/get_case", methods=["POST"])
        def get_case():
            worker_id = request.json["worker_id"]
            with self._lock:

                # Queued/non-failed cases are dispatched first
                queued_cases = self._get_cases_by_state(State.QUEUED)
                if queued_cases:
                    case = queued_cases[0]
                    case.state = State.RUNNING
                    case.timestamp = time()
                    case.worker_ids.append(worker_id)
                    info(f"Case {case.id} assigned to worker {worker_id}.")
                    return {"doe_name": self._doe_name, "case_id": case.id}

                # If there are no more queued cases to dispatch, then we go to failed
                failed_cases = self._get_cases_by_state(State.FAILED)
                if failed_cases:
                    for case in failed_cases:
                        # We only assign a failed case to a worker if it has not
                        # already attempted to run it
                        if worker_id not in case.worker_ids:
                            case.state = State.RUNNING
                            case.timestamp = time()
                            case.worker_ids.append(worker_id)
                            info(
                                f"Failed case {case.id} assigned to worker {worker_id}."
                            )
                            return {
                                "doe_name": self._doe_name,
                                "case_id": case.id,
                            }

                # If there are still running cases that may fail, we let the worker
                # know (as it may want to stand by)
                running_cases = self._get_cases_by_state(State.RUNNING)
                if running_cases:
                    info(
                        f"Worker {worker_id} requested a case but none is queued."
                        " It was notified that some cases are still running."
                    )
                    return {
                        "doe_name": self._doe_name,
                        "case_id": None,
                        "comment": "cases_still_running",
                    }
                else:
                    info(
                        f"Worker {worker_id} requested a case but none is queued."
                        " It was notified that no cases are running."
                    )
                    return {
                        "doe_name": self._doe_name,
                        "case_id": None,
                        "comment": "no_cases_running",
                    }

        @app.route("/report_alive", methods=["POST"])
        def report_alive():
            worker_id = request.json["worker_id"]
            case_id = request.json["case_id"]
            with self._lock:
                self._cases[case_id].timestamp = time()
            info(f"Worker {worker_id} reported that case {case_id} is alive.")
            return {}

        @app.route("/report_failed", methods=["POST"])
        def report_failed():
            worker_id = request.json["worker_id"]
            case_id = request.json["case_id"]
            with self._lock:
                self._cases[case_id].state = State.FAILED
            info(f"Worker {worker_id} reported that case {case_id} failed.")
            return {}

        @app.route("/report_finished", methods=["POST"])
        def report_finished():
            worker_id = request.json["worker_id"]
            case_id = request.json["case_id"]
            with self._lock:
                self._cases[case_id].state = State.FINISHED
            info(f"Worker {worker_id} reported that case {case_id} finished.")
            return {}

        @app.route("/progress_report", methods=["GET"])
        def progress_report():
            with self._lock:
                queued_cases = self._get_cases_by_state(State.QUEUED)
                running_cases = self._get_cases_by_state(State.RUNNING)
                failed_cases = self._get_cases_by_state(State.FAILED)
                finished_cases = self._get_cases_by_state(State.FINISHED)
            return (
                "<p>Queued cases: "
                + ", ".join([str(case.id) for case in queued_cases])
                + "</p><p>Running cases: "
                + ", ".join(
                    [f"{case.id} (W. {case.worker_ids[-1]})" for case in running_cases]
                )
                + "</p><p>Failed cases: "
                + ", ".join(
                    [
                        f"{case.id} (W. {', '.join(case.worker_ids)})"
                        for case in failed_cases
                    ]
                )
                + "</p><p>Finished cases: "
                + ", ".join(
                    [f"{case.id} (W. {case.worker_ids[-1]})" for case in finished_cases]
                )
                + "</p>"
            )

        @app.route("/progress_table", methods=["GET"])
        def progress_table():
            return render_template("progress_table.html")

        @app.route("/list_cases", methods=["GET"])
        def list_cases():
            return Response(
                json.dumps(
                    [
                        {
                            "id": case.id,
                            "state": case.state.name,
                            "timestamp": datetime.fromtimestamp(
                                case.timestamp
                            ).strftime("%Y%m%dT%H%M%S")
                            if case.timestamp is not None
                            else None,
                            "worker_id": case.worker_ids[-1]
                            if case.worker_ids
                            else None,
                        }
                        for case in self._cases.values()
                    ]
                ),
                mimetype="application/json",
            )

        # Timer used to check for timed out cases
        self._timer = Timer(self._check_interval, self._monitor_timed_out_cases)
        self._timer.start()

        # Quiet down Flask
        getLogger("werkzeug").setLevel(ERROR)

        # Start app
        serve(app, host=self._host, port=self._port)
