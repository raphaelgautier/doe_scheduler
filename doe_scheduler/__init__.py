from pathlib import Path

logs_path = Path(__file__).parents[1] / "logs"
logs_path.mkdir(parents=True, exist_ok=True)
