from logging import basicConfig, FileHandler, StreamHandler, info, error, INFO, ERROR
from sys import stdout
from threading import Lock, Timer

from requests import post

from doe_scheduler import logs_path

# Logging config
basicConfig(
    format="%(asctime)s %(levelname)s:%(message)s",
    level=ERROR,
    handlers={FileHandler(logs_path / "client.log", "a+"), StreamHandler(stdout)},
)


class DoESchedulerClient:
    def __init__(self, server, name, interval=30):
        self._server = server
        self._id = name
        self._interval = interval
        self._doe_name = None
        self._case_id = None
        self._timer = None
        self._lock = Lock()

    def get_case(self):
        res = post(f"{self._server}/get_case", json={"worker_id": self._id}).json()
        with self._lock:
            self._doe_name, self._case_id = res["doe_name"], res["case_id"]
            if self._case_id is not None:
                info(
                    "Worker {} has been assigned case {} from DOE {}.".format(
                        self._id, self._case_id, self._doe_name
                    )
                )
                self._start_timer()
            else:
                info(f"Worker {self._id} has requested a new case but none was given.")
        return self._doe_name, self._case_id

    def report_failed(self):
        with self._lock:
            error(f"Worker {self._id} reporting that " f"case {self._case_id} failed.")
            post(
                f"{self._server}/report_failed",
                json={"worker_id": self._id, "case_id": self._case_id},
            )
            self._reset()

    def report_finished(self):
        with self._lock:
            info(f"Worker {self._id} reporting that " f"case {self._case_id} finished.")
            post(
                f"{self._server}/report_finished",
                json={"worker_id": self._id, "case_id": self._case_id},
            )
            self._reset()

    def _start_timer(self):
        self._timer = Timer(self._interval, self._report_alive)
        self._timer.daemon = True
        self._timer.start()

    def _reset(self):
        if self._timer is not None:
            self._timer.cancel()
        self._doe_name = None
        self._case_id = None
        self._timer = None

    def _report_alive(self):
        with self._lock:
            if self._id is not None and self._case_id is not None:
                info(f"Worker {self._id} reporting that case {self._case_id} is alive.")
                post(
                    f"{self._server}/report_alive",
                    json={"worker_id": self._id, "case_id": self._case_id},
                )
                self._start_timer()
