from setuptools import setup

setup(
    name="DOE Scheduler",
    version="1.0",
    url="https://gitlab.com/raphael.gautier/doe_scheduler",
    author="Raphaël Gautier",
    author_email="raphael.gautier@posteo.net",
    description="A simple REST-based scheduler for running DOEs in a distributed manner.",
    packages=["doe_scheduler"],
    package_data={'doe_scheduler': ['templates/progress_table.html']},
    install_requires=["requests", "waitress", "flask", "flask_cors"],
)
